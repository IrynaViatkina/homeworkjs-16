const lightTheme = 'light';
const darkTheme = 'dark';

if(!localStorage.getItem('theme')){
	localStorage.setItem('theme', lightTheme);
}
else{
	let link = document.createElement('link');
	link.rel = 'stylesheet';
	link.href = `css/${localStorage.getItem('theme')}.css`;
	document.head.appendChild(link);
}

document.addEventListener('click', function(e){
	if(e.target.id == 'change'){
		document.head.appendChild(changeTheme());
	}
});

function changeTheme(){
	let link = document.createElement('link');
	link.rel = 'stylesheet';
	
	let theme = localStorage.getItem('theme');
	if(theme == 'light'){
		localStorage.setItem('theme', darkTheme);
		link.href = 'css/dark.css';
	}
	else{
		localStorage.setItem('theme', lightTheme);
		link.href = 'css/light.css';
	}
	return link;
}